FROM nginx
COPY ./build/client/emscripten /usr/share/web
COPY ./web /usr/share/web
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./assets /usr/share/web/assets
EXPOSE 80
