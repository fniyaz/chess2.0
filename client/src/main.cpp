#include <emscripten.h>
#include <emscripten/html5.h>
#include <emscripten/websocket.h>
#include <GLES2/gl2.h>
#include <stdio.h>
#include <math.h>
#include <cstring>
#include <vector>
#include <string>
#include <optional>
#include <linalg.h>
#include <iostream>
#include <sstream>
using namespace linalg::ostream_overloads;


template<typename T, size_t N, size_t M>
void mat2arr(linalg::mat<T, N, M> matrix, T arr[]) {
  for(size_t r = 0; r < N; r++) {
    auto row = matrix.row(r);
    for(size_t c = 0; c < M; c++) {
      arr[c*N + r] = row[c];
    }
  }
}


const float faces[] {
  -1.0, -1.0, 1.0,
  1.0, -1.0, 1.0,
  1.0, 1.0, 1.0,
  -1.0, 1.0, 1.0,

  -1.0, -1.0, -1.0,
  -1.0, 1.0, -1.0,
  1.0, 1.0, -1.0,
  1.0, -1.0, -1.0, // Back face

  -1.0, 1.0, -1.0,
  -1.0, 1.0, 1.0,
  1.0, 1.0, 1.0,
  1.0, 1.0, -1.0, // Top face

  -1.0, -1.0, -1.0,
  1.0, -1.0, -1.0,
  1.0, -1.0, 1.0,
  -1.0, -1.0, 1.0, // Bottom face

  1.0, -1.0, -1.0,
  1.0, 1.0, -1.0,
  1.0, 1.0, 1.0,
  1.0, -1.0, 1.0, // Right face

  -1.0, -1.0, -1.0,
  -1.0, -1.0, 1.0,
  -1.0, 1.0, 1.0,
  -1.0, 1.0, -1.0, // Left face

  // 1.0, 1.0, 0.0,
  // -1.0, 1.0, 0.0,
  // 0.0, -1.0, 0.0,
};

const float normals[] {
  0.0, 0.0, 1.0,
  0.0, 0.0, 1.0,
  0.0, 0.0, 1.0,
  0.0, 0.0, 1.0,

  0.0, 0.0, -1.0,
  0.0, 0.0, -1.0,
  0.0, 0.0, -1.0,
  0.0, 0.0, -1.0,

  0.0, 1.0, 0.0,
  0.0, 1.0, 0.0,
  0.0, 1.0, 0.0,
  0.0, 1.0, 0.0,

  0.0, -1.0, 0.0,
  0.0, -1.0, 0.0,
  0.0, -1.0, 0.0,
  0.0, -1.0, 0.0,

  1.0, 0.0, 0.0,
  1.0, 0.0, 0.0,
  1.0, 0.0, 0.0,
  1.0, 0.0, 0.0,

  -1.0, 0.0, 0.0,
  -1.0, 0.0, 0.0,
  -1.0, 0.0, 0.0,
  -1.0, 0.0, 0.0,

  // 1.0, 1.0, 0.0,
  // -1.0, 1.0, 0.0,
  // 0.0, -1.0, 0.0,
};

const unsigned short indices[] = {
  0, 1, 2, 0, 2, 3, // front
  4, 5, 6, 4, 6, 7, // back
  8, 9, 10, 8, 10, 11, // top
  12, 13, 14, 12, 14, 15, // bottom
  16, 17, 18, 16, 18, 19, // right
  20, 21, 22, 20, 22, 23, // left

  // 0, 1, 2,
  // 0, 2, 1,
};

const char* vsSource = R"--(
attribute vec4 aVertexPosition;
attribute vec3 aVertexNormal;

uniform mat4 uModelViewMatrix;
uniform mat4 uNormalsMatrix;
uniform mat4 uProjectionMatrix;

varying highp vec3 vNormal;


void main(void) {
  gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
  vec4 normal4 = uNormalsMatrix * vec4(aVertexNormal, 1.0);
  vNormal = normal4.xyz / normal4.w;
}
)--";

const char* fsSource = R"--(
precision highp float;

varying highp vec3 vNormal;

void main(void) {
  vec3 light_dir = normalize(vec3(-0.7, 0.5, 1.0));
  vec3 light_color = vec3(0.0, 1.0, 1.0);
  vec3 ambient_color = vec3(0.984, 0.4627, 0.502);
  float diff = clamp(dot(vNormal, light_dir), 0.0, 1.0);
  gl_FragColor = vec4(light_color * diff + ambient_color * 0.3 , 1.0);

  // gl_FragColor = vec4(vNormal*0.5 + vec3(0.5), 1.0);
}
)--";


std::optional<GLuint> prepare_shader(const char* code, GLenum type) {
  auto shader = glCreateShader(type);
  GLint len = strlen(code);
  glShaderSource(shader, 1, &code, &len);
  glCompileShader(shader);

  GLint status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (status != GL_TRUE) {
    GLint logSize;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
    std::vector<GLchar> log(logSize);
    glGetShaderInfoLog(shader, logSize, &logSize, log.data());
    emscripten_err(log.data());

    glDeleteShader(shader);

    return {};
  }

  return {shader};
}


std::optional<GLint> initShaders() {
  auto vertexShader = prepare_shader(vsSource, GL_VERTEX_SHADER);
  auto fragmentShader = prepare_shader(fsSource, GL_FRAGMENT_SHADER);
  if(!vertexShader.has_value() || !fragmentShader.has_value()) {
    return  {};
  }

  auto program = glCreateProgram();
  glAttachShader(program, vertexShader.value());
  glAttachShader(program, fragmentShader.value());
  glLinkProgram(program);

  GLint status;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  if (status != GL_TRUE) {
    GLint logSize;
    glGetProgramiv(program, GL_LINK_STATUS, &logSize);
    std::vector<GLchar> log(logSize);
    glGetProgramInfoLog(program, logSize, &logSize, log.data());
    emscripten_err(log.data());

    glDeleteProgram(program);

    return {};
  }

  return program;
}


struct MouseState {
  bool down;
  linalg::vec<float, 2> lastPos;
};

struct Singleton{
  GLint shaderProgram;
  GLint vertexPosAttr;
  GLint normalPosAttr;
  GLint projMatrixLocation;
  GLint normalsMatrixLocation;
  GLint modelViewMatrixLocation;
  GLint vertexBuffer;
  GLint normalBuffer;
  GLint indexBuffer;
  int websocket;
  MouseState mouseState;
  linalg::vec<float, 2> rotation;
};



linalg::vec<float, 4> calc_rotation(linalg::vec<float, 2> delta) {
  return qmul(
      linalg::rotation_quat<float>(linalg::vec<float, 3>(1.0, 0.0, 0.0), delta.y/500),
      linalg::rotation_quat<float>(linalg::vec<float, 3>(0.0, 1.0, 0.0), delta.x/500)
  );
}

void render(Singleton& data, double time) {
  glEnable(GL_DEPTH_TEST);
  glClearColor(0.984, 0.4627, 0.502, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  float fieldOfView = (45. * M_PI) / 180.; // in radians
  float aspect = 1.0;
  float zNear = 0.1;
  float zFar = 100.0;

  linalg::mat<float, 4, 4> projectionMatrix = linalg::perspective_matrix<float>(fieldOfView, aspect, zNear, zFar);
  float projectionArr[16];
  mat2arr<float, 4, 4>(projectionMatrix, projectionArr);

  linalg::mat<float, 4, 4> modelViewMatrix = linalg::translation_matrix(linalg::vec<float, 3>(0.0, 0.0, -6.0));
  auto rotation_matrix = linalg::rotation_matrix(calc_rotation(data.rotation));
  modelViewMatrix = mul(modelViewMatrix, rotation_matrix);
  float modelViewArr[16];
  mat2arr<float, 4, 4>(modelViewMatrix, modelViewArr);
  float normalsMatrixArr[16];
  mat2arr<float, 4, 4>(rotation_matrix, normalsMatrixArr);

  glBindBuffer(GL_ARRAY_BUFFER, data.vertexBuffer);
  glVertexAttribPointer(data.vertexPosAttr, 3, GL_FLOAT, false, 0, 0);
  glEnableVertexAttribArray(data.vertexPosAttr);

  glBindBuffer(GL_ARRAY_BUFFER, data.normalBuffer);
  glVertexAttribPointer(data.normalPosAttr, 3, GL_FLOAT, false, 0, 0);
  glEnableVertexAttribArray(data.normalPosAttr);


  glUseProgram(data.shaderProgram);

  glUniformMatrix4fv(data.projMatrixLocation, 1, 0, projectionArr);
  glUniformMatrix4fv(data.modelViewMatrixLocation, 1, 0, modelViewArr);
  glUniformMatrix4fv(data.normalsMatrixLocation, 1, 0, normalsMatrixArr);


  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data.indexBuffer);
  glDrawElements(GL_TRIANGLES, sizeof(indices)/sizeof(unsigned short), GL_UNSIGNED_SHORT, 0);
}

bool setup_webgl(Singleton& singleton) {
  EmscriptenWebGLContextAttributes attr;
  emscripten_webgl_init_context_attributes(&attr);
  attr.alpha = 1;
  attr.depth = 1;

  EMSCRIPTEN_WEBGL_CONTEXT_HANDLE ctx = emscripten_webgl_create_context("#canvas", &attr);
  emscripten_webgl_make_context_current(ctx);

  auto shaderProgram = initShaders();
  if (!shaderProgram.has_value())
    return false;


  GLuint buffer[3];
  glGenBuffers(3, buffer);
  auto vertex_buffer = buffer[0];
  auto normal_buffer = buffer[1];
  auto index_buffer = buffer[2];

  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(faces), faces, GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


  auto aVertexPosAttr = glGetAttribLocation(shaderProgram.value(), "aVertexPosition");
  auto aNormalPosAttr = glGetAttribLocation(shaderProgram.value(), "aVertexNormal");
  auto projMatrixLocation = glGetUniformLocation(shaderProgram.value(), "uProjectionMatrix");
  auto normalsMatrixLocation = glGetUniformLocation(shaderProgram.value(), "uNormalsMatrix");
  auto modelViewMatrixLocation = glGetUniformLocation(shaderProgram.value(), "uModelViewMatrix");


  singleton.shaderProgram = shaderProgram.value();
  singleton.vertexPosAttr = aVertexPosAttr;
  singleton.normalPosAttr = aNormalPosAttr;
  singleton.projMatrixLocation = projMatrixLocation;
  singleton.normalsMatrixLocation = normalsMatrixLocation;
  singleton.modelViewMatrixLocation = modelViewMatrixLocation;
  singleton.vertexBuffer = vertex_buffer;
  singleton.normalBuffer = normal_buffer;
  singleton.indexBuffer = index_buffer;

  return true;
}

void change_rotation(Singleton* singleton, linalg::vec<float, 2> dr) {
    singleton->rotation += dr;
    std::stringstream ss;
    ss << dr.x << ", " << dr.y;
    emscripten_websocket_send_utf8_text(singleton->websocket, ss.str().c_str());
}

void setup_mouse_callbacks(Singleton* singleton) {
  em_mouse_callback_func mouse_callback = [](int eventtype, const EmscriptenMouseEvent* mouseEvent, void* data){
    Singleton* singleton = static_cast<Singleton*>(data);

    switch(eventtype) {
      case EMSCRIPTEN_EVENT_MOUSEDOWN:
        singleton->mouseState.down = true;
        singleton->mouseState.lastPos = {mouseEvent->clientX, mouseEvent->clientY};
        break;
      case EMSCRIPTEN_EVENT_MOUSEUP:
        singleton->mouseState.down = false;
        break;
      case EMSCRIPTEN_EVENT_MOUSEMOVE:
        if (singleton->mouseState.down) {
          auto old_pos = singleton->mouseState.lastPos;
          singleton->mouseState.lastPos = {mouseEvent->clientX, mouseEvent->clientY};
          change_rotation(singleton, singleton->mouseState.lastPos - old_pos);
        }

        break;
    }
    return EM_TRUE;
  };
  em_touch_callback_func touch_callback = [](int eventtype, const EmscriptenTouchEvent* mouseEvent, void* data){
    Singleton* singleton = static_cast<Singleton*>(data);

    switch(eventtype) {
      case EMSCRIPTEN_EVENT_TOUCHSTART:
        if (mouseEvent->numTouches > 0) {
          singleton->mouseState.down = true;
          singleton->mouseState.lastPos = {mouseEvent->touches[0].clientX, mouseEvent->touches[0].clientY};
        }
        break;
      case EMSCRIPTEN_EVENT_TOUCHEND:
      case EMSCRIPTEN_EVENT_TOUCHCANCEL:
        if (mouseEvent->numTouches == 0)
          singleton->mouseState.down = false;
        break;
      case EMSCRIPTEN_EVENT_TOUCHMOVE:
        if (singleton->mouseState.down) {
          auto old_pos = singleton->mouseState.lastPos;
          singleton->mouseState.lastPos = {mouseEvent->touches[0].clientX, mouseEvent->touches[0].clientY};
          change_rotation(singleton, singleton->mouseState.lastPos - old_pos);
        }

        break;
    }
    return EM_TRUE;
  };
  emscripten_set_mousedown_callback("#canvas", singleton, false, mouse_callback);
  emscripten_set_mousemove_callback(EMSCRIPTEN_EVENT_TARGET_DOCUMENT, singleton, false, mouse_callback);
  emscripten_set_mouseup_callback(EMSCRIPTEN_EVENT_TARGET_DOCUMENT, singleton, false, mouse_callback);

  emscripten_set_touchstart_callback("#canvas", singleton, false, touch_callback);
  emscripten_set_touchmove_callback(EMSCRIPTEN_EVENT_TARGET_DOCUMENT, singleton, false, touch_callback);
  emscripten_set_touchend_callback(EMSCRIPTEN_EVENT_TARGET_DOCUMENT, singleton, false, touch_callback);
  emscripten_set_touchcancel_callback(EMSCRIPTEN_EVENT_TARGET_DOCUMENT, singleton, false, touch_callback);
}

EM_JS(char*, getlocationunsafe, (), {
    let length = lengthBytesUTF8(window.location.hostname) + 1;
    let str = _malloc(length);
    stringToUTF8(window.location.hostname, str, length);
    return str;
});

std::string getlocation() {
  auto location = getlocationunsafe();
  std::string locationstr(location);
  free(location);

  return locationstr;
}

bool setup_websocket(Singleton* singleton) {
  EmscriptenWebSocketCreateAttributes attr;
  emscripten_websocket_init_create_attributes(&attr);
  auto loc = getlocation();
  std::stringstream ss;
  ss << "ws://" << loc << ":8090" << std::endl;
  attr.url = ss.str().c_str();

  auto socket = emscripten_websocket_new(&attr);
  if (socket <= 0)
    return false;

  // emscripten_websocket_set_onopen_callback(socket, userData, callback);
  auto onmessage_callback = [](int eventType, const EmscriptenWebSocketMessageEvent* event, void* data){
    Singleton* singleton = static_cast<Singleton*>(data);

    std::istringstream is(std::string((char*)(event->data)));
    float dx;
    char c;
    float dy;
    is >> dx >> c >> dy;

    emscripten_dbg((char*)event->data);
    std::stringstream ss;
    ss << dx << " " << dy << std::endl;
    emscripten_dbg((char*)ss.str().c_str());
    singleton->rotation = {dx, dy};

    return EM_FALSE;
  };
  emscripten_websocket_set_onmessage_callback(socket, singleton, onmessage_callback);
  emscripten_websocket_set_onerror_callback(socket, singleton, [](int eventType, const EmscriptenWebSocketErrorEvent* event, void* data){
    // Singleton* singleton = static_cast<Singleton*>(data);

    emscripten_err("Socket error");

    // todo emscripten_set_timeout()
    return EM_FALSE;
  });
  // emscripten_websocket_set_onclose_callback(socket, userData, callback);
  singleton->websocket = socket;

  return true;
}

int main() {
  Singleton* singleton = new Singleton();

  setup_websocket(singleton);

  setup_webgl(*singleton);
  emscripten_request_animation_frame_loop([](double time, void* data){
      Singleton* singleton = (Singleton*)data;
      render(*singleton, time);
      return EM_TRUE;
  }, singleton);

  setup_mouse_callbacks(singleton);
}

