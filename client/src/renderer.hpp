#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include <linalg.h>

#pragma GCC diagnostic pop

#include <cassert>
#include <filesystem>
#include <memory>
#include <optional>
#include <span>

namespace linalg {

template <typename... T>
vec(T...) -> vec<std::common_type_t<T...>, sizeof...(T)>;

}

enum class AttributeLocation : uint32_t { Positions, Normals, TexCoords };

struct InstanceData {
  linalg::vec<float, 3> position;
};

class InstancedMesh;
class Renderer;

class Texture {
 public:
  static std::unique_ptr<Texture> create(size_t w, size_t h,
                                         std::span<const uint8_t> data);

  void bind();

 private:
  explicit Texture(uint32_t texture);

  uint32_t texture;
};

class Material {
 public:
  explicit Material(uint32_t program);

  void bind() const;

  void set_texture(std::shared_ptr<Texture> texture);

  void update_model_matrix(const linalg::aliases::float4x4& matrix,
                           const linalg::aliases::float4x4& inv_matrix);
  void update_camera_position(linalg::aliases::float3 pos);

 private:
  uint32_t program;

  std::optional<std::shared_ptr<Texture>> texture;

  int32_t camera_position_location;
  int32_t model_matrix_location;
  int32_t inv_model_matrix_location;
};

class Mesh {
 public:
  Mesh(uint32_t vertex_num, uint32_t vertex_buffer)
      : vertex_num{vertex_num}, vertex_buffer{vertex_buffer} {}

  void bind_vertex_buffer() const;

  uint32_t get_vertex_num() const { return vertex_num; }

 private:
  uint32_t vertex_num;
  uint32_t vertex_buffer;
};

class InstancedMesh {
 public:
  InstancedMesh(std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material)
      : mesh{mesh}, material{material} {
    assert(mesh != nullptr);
    assert(material != nullptr);
  }

  void render();

  void translate(linalg::aliases::float3 offset) { pos += offset; }
  void rotate(linalg::aliases::float4 rotation) {
    this->rotation = linalg::qmul(this->rotation, rotation);
  }

 private:
  std::shared_ptr<Mesh> mesh;
  std::shared_ptr<Material> material;
  linalg::aliases::float3 pos{};
  linalg::aliases::float4 rotation{0.0f, 0.0f, 0.0f, 1.0f};
  linalg::aliases::float3 scale{1.0f};
};

class Environment {
 public:
  virtual ~Environment() = default;
  virtual double time_now() const = 0;
  virtual float aspect_ratio() const = 0;

  struct Texture {
    size_t w, h;
    int format;
    std::vector<uint8_t> data;
  };

  virtual Texture load_texture(std::filesystem::path const& path) const = 0;
};

class Camera {
 public:
  void translate(linalg::aliases::float3 offset) { position += offset; }

  void rotate(linalg::aliases::float4 quat) { rotation = qmul(quat, rotation); }

  void look_at(linalg::aliases::float3 point) {
    auto dir = linalg::normalize(point - position);
    auto axis = linalg::cross({0.0f, 0.0f, -1.0f}, dir);
    auto angle = std::acos(linalg::dot({0.0f, 0.0f, -1.0f}, dir));
    rotation = linalg::rotation_quat(axis, angle);
  }

  auto calc_view_matrix() const {
    auto dir = linalg::qrot(rotation, linalg::vec{0.0f, 0.0f, -1.0f});
    auto up = linalg::vec{0.0f, 1.0f, 0.0f};
    return linalg::lookat_matrix(position, position + dir, up);
  }

  // convert a vector from global space to local space of the camera
  auto dir_to_local(linalg::vec<float, 3> global) const {
    return linalg::qrot(rotation, global);
  }

  auto get_position() const { return position; }

 private:
  linalg::aliases::float3 position{};
  linalg::aliases::float4 rotation{0.0, 0.0, 0.0, 1.0};
};

class Renderer : public std::enable_shared_from_this<Renderer> {
 public:
  static std::shared_ptr<Renderer> create(std::unique_ptr<Environment> env);

  ~Renderer();

  std::unique_ptr<Mesh> load_mesh(const std::filesystem::path& path);
  std::shared_ptr<InstancedMesh> instantiate_mesh(const std::shared_ptr<Mesh>&);

  std::shared_ptr<Texture> load_texture(std::filesystem::path const&);
  void set_main_texture(std::shared_ptr<Texture> texture);

  void render_frame(double now);

  void resize(double new_width, double new_height);

  Camera& get_camera() { return camera; }

 private:
  std::unique_ptr<Environment> env;

  std::vector<std::shared_ptr<InstancedMesh>> meshes;

  Camera camera;

  double last_frame_start{};
  double last_frame_end{};
  float aspect_ratio{};

  uint32_t program;
  uint32_t view_proj_matrix_location;
  std::shared_ptr<Material> main_material;
};