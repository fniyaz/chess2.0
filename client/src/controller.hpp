#pragma once

#include "renderer.hpp"

enum class Key {
  W,
  A,
  S,
  D,
  Q,
  E,

  Space,
  Esc,
  Enter,

  ArrowUp,
  ArrowDown,
  ArrowLeft,
  ArrowRight,

  Shift,
  Ctrl,
  Alt,

  One,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Zero,
};

enum class MouseKey { Left, Right, Middle };

enum class KeyState { Pressed, Released };

class Controller {
 public:
  explicit Controller(std::shared_ptr<Renderer> renderer, std::filesystem::path asset_path)
      : renderer{renderer}, asset_path{asset_path} {}

  void process_keyboard_input(Key key, KeyState state) {
    switch (state) {
      case KeyState::Pressed: {
        switch (key) {
          case Key::W:
            camera_velocity.z = -camera_speed;
            break;
          case Key::S:
            camera_velocity.z = camera_speed;
            break;
          case Key::A:
            camera_angular_velocity.y = camera_angular_speed;
            break;
          case Key::D:
            camera_angular_velocity.y = -camera_angular_speed;
            break;
          case Key::Q:
            camera_velocity.y = camera_speed;
            break;
          case Key::E:
            camera_velocity.y = -camera_speed;
            break;
          default:
            break;
        }
        break;
      }
      case KeyState::Released: {
        switch (key) {
          case Key::W:
            camera_velocity.z = 0.0;
            break;
          case Key::S:
            camera_velocity.z = 0.0;
            break;
          case Key::A:
            camera_angular_velocity.y = 0.0;
            break;
          case Key::D:
            camera_angular_velocity.y = 0.0;
            break;
          case Key::Q:
            camera_velocity.y = 0.0;
            break;
          case Key::E:
            camera_velocity.y = 0.0;
            break;
          default:
            break;
        }
        break;
      }
    }
  }

  void process_mouse_key(MouseKey, KeyState) {}
  void process_mouse_scroll(float) {}
  void process_mouse_move(float, float, float, float) {}

  void init_scene() {
    std::shared_ptr<Mesh> knight = renderer->load_mesh(asset_path / "knight.obj");
    std::shared_ptr<Mesh> bishop = renderer->load_mesh(asset_path / "bishop.obj");
    std::shared_ptr<Mesh> pawn = renderer->load_mesh(asset_path / "pawn.obj");
    std::shared_ptr<Mesh> queen = renderer->load_mesh(asset_path / "queen.obj");
    std::shared_ptr<Mesh> rook = renderer->load_mesh(asset_path / "rook.obj");
    std::shared_ptr<Mesh> king = renderer->load_mesh(asset_path / "king.obj");
    std::shared_ptr<Mesh> board = renderer->load_mesh(asset_path / "board.obj");

    auto texture = renderer->load_texture(asset_path / "test_texture.jpg");
    renderer->set_main_texture(texture);

    // clang-format off
    std::shared_ptr<Mesh> piece_order[] {
        rook, knight, bishop, queen, king, bishop, knight, rook,
        pawn, pawn, pawn, pawn, pawn, pawn, pawn, pawn, 
        pawn, pawn, pawn, pawn, pawn, pawn, pawn, pawn, 
        rook, knight, bishop, queen, king, bishop, knight, rook,
    };
    // clang-format on

    float z_coord[4] {-10.0, -12.0, -20.0, -22.0};
    size_t idx{};
    for (auto& piece_mesh: piece_order) {
        size_t column = idx % 8;
        size_t row = idx / 8;
        auto piece = renderer->instantiate_mesh(piece_mesh);
        pieces[idx] = piece;
        piece->translate(linalg::vec{column * 1.7f, -10.0f, z_coord[row]});
        idx++;
    }
    this->board = renderer->instantiate_mesh(board);
    this->board->translate(linalg::vec{6.0f, -12.0f, -16.0f});
  }

  void update(float delta_time_sec) {
    renderer->get_camera().translate(
        renderer->get_camera().dir_to_local(camera_velocity) * delta_time_sec);
    renderer->get_camera().rotate(
        linalg::rotation_quat(linalg::vec{0.0f, 1.0f, 0.0f},
                              camera_angular_velocity.y * delta_time_sec));
    for (auto& piece: pieces) {
        piece->rotate(linalg::rotation_quat(linalg::vec{0.0f, 1.0f, 0.0f}, 1.0f * delta_time_sec));
    }
    board->rotate(linalg::rotation_quat(linalg::vec{0.0f, 1.0f, 0.0f}, 1.0f * delta_time_sec));
  }

 private:
  std::shared_ptr<Renderer> renderer;
  std::filesystem::path asset_path;

  float camera_speed = 20.0;
  float camera_angular_speed = 3.0;
  linalg::aliases::float3 camera_velocity{};
  linalg::aliases::float3 camera_angular_velocity{};

  std::span<std::shared_ptr<InstancedMesh>> whites() {
    return std::span{pieces}.subspan(0, 16);
  }

  std::span<std::shared_ptr<InstancedMesh>> blacks() {
    return std::span{pieces}.subspan(16, 32);
  }

  std::array<std::shared_ptr<InstancedMesh>, 32> pieces;
  std::shared_ptr<InstancedMesh> board;
};