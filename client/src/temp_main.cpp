#include <SDL2/SDL_image.h>
#include <emscripten/html5.h>

#include <fstream>
#include <iostream>

#include "controller.hpp"
#include "renderer.hpp"

using namespace std::string_literals;

EM_JS(float, getWidth, (), { return document.querySelector("#canvas").width; })

EM_JS(float, getHeight, (),
      { return document.querySelector("#canvas").height; })

class EmscriptenEnvironment : public Environment {
 public:
  double time_now() const override {
    return emscripten_performance_now() / 1e6;
  }

  float aspect_ratio() const override {
    float inner_width = getWidth(), inner_height = getHeight();
    return inner_width / inner_height;
  }

  Texture load_texture(std::filesystem::path const &path) const override {
    SDL_Surface *surface = IMG_Load(path.c_str());
    if (surface == nullptr) {
      std::cerr << "Failed to load texture from " << path << ": "
                << IMG_GetError() << "\n";
      throw std::runtime_error{"Texture load failed"};
    }
    SDL_Surface* rgba = SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_RGBA32, 0);
    std::vector<uint8_t> res;
    size_t size = rgba->w * rgba->h * rgba->format->BytesPerPixel;
    res.resize(size);
    std::copy_n(static_cast<uint8_t *>(rgba->pixels), size, res.begin());
    Texture texture{
        .w = static_cast<size_t>(rgba->w),
        .h = static_cast<size_t>(rgba->h),
        .format = 42,
        .data = std::move(res),
    };
    SDL_FreeSurface(surface);
    SDL_FreeSurface(rgba);
    return texture;
  }
};

static std::shared_ptr<Renderer> globalRenderer;
static std::shared_ptr<Controller> controller;

void run_frame_loop(void *renderer) {
  emscripten_request_animation_frame_loop(
      [](double now_ms, void *renderer) -> EM_BOOL {
        reinterpret_cast<Renderer *>(renderer)->render_frame(now_ms / 1e3);
        double frame_time_ms = emscripten_performance_now() - now_ms;
        controller->update(frame_time_ms / 1e3);
        return true;
      },
      renderer);
}

void fetch_files_async(std::vector<std::string_view> file_names,
                       std::function<void()> on_complete_) {
  static size_t files_left = 0;
  static std::function<void()> on_complete;
  on_complete = std::move(on_complete_);
  assert(files_left == 0);
  files_left = file_names.size();
  for (auto &file : file_names) {
    auto full_file_name = "/" + std::string(file);
    emscripten_async_wget(
        file.data(), full_file_name.data(),
        [](const char *file) {
          std::cerr << "Loaded " << file << "\n";
          files_left--;
          if (files_left == 0) {
            on_complete();
          }
        },
        [](const char *msg) {
          files_left--;
          std::cerr << "Failed to fetch file: " << msg << "\n";
        });
  }
}

int main() {
  EmscriptenWebGLContextAttributes attrs{};
  emscripten_webgl_init_context_attributes(&attrs);
  attrs.alpha = true;
  attrs.powerPreference = EM_WEBGL_POWER_PREFERENCE_HIGH_PERFORMANCE;
  attrs.failIfMajorPerformanceCaveat = true;
  attrs.majorVersion = 2;
  attrs.minorVersion = 0;

  auto ctx = emscripten_webgl_create_context("#canvas", &attrs);
  if (ctx < 0) {
    throw std::runtime_error{"Failed to create webgl context: "s +
                             std::to_string(ctx)};
  }
  auto res = emscripten_webgl_make_context_current(ctx);
  if (res < 0) {
    throw std::runtime_error{"Failed to make webgl context current: "s +
                             std::to_string(ctx)};
  }

  auto renderer = Renderer::create(std::make_unique<EmscriptenEnvironment>());
  globalRenderer = renderer;

  controller = std::make_shared<Controller>(renderer, "/assets");

  EmscriptenFullscreenStrategy strategy{
      .scaleMode = EMSCRIPTEN_FULLSCREEN_SCALE_STRETCH,
      .canvasResolutionScaleMode = EMSCRIPTEN_FULLSCREEN_CANVAS_SCALE_HIDEF,
      .filteringMode = EMSCRIPTEN_FULLSCREEN_FILTERING_BILINEAR,
      .canvasResizedCallback = [](int, const void *, void *renderer) -> int {
        reinterpret_cast<Renderer *>(renderer)->resize(getWidth(), getHeight());
        return 0;
      },
      .canvasResizedCallbackUserData = renderer.get(),
  };
  emscripten_enter_soft_fullscreen("#canvas", &strategy);

  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
    std::cerr << "Failed to initialize SDL: " << SDL_GetError() << "\n";
    return -1;
  }

  int img_init_res = IMG_Init(IMG_INIT_JPG);
  if ((img_init_res & IMG_INIT_JPG) == 0) {
    std::cerr << "Failed to initialize SDL image: " << IMG_GetError() << "\n";
    return -1;
  }

  fetch_files_async({"assets/knight.obj", "assets/pawn.obj", "assets/queen.obj",
                     "assets/king.obj", "assets/bishop.obj", "assets/rook.obj",
                     "assets/board.obj", "assets/test_texture.jpg"},
                    []() { controller->init_scene(); });
  run_frame_loop(renderer.get());
  return 0;
}