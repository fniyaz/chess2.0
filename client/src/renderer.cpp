#include "renderer.hpp"

#include <GLES3/gl3.h>
#include <tiny_obj_loader.h>

#include <cassert>
#include <iostream>
#include <type_traits>
#include <ranges>

#include "shaders/blinn_phong.frag.hpp"
#include "shaders/blinn_phong.vert.hpp"

using namespace linalg::ostream_overloads;
using namespace std::string_literals;

struct Vertex {
  float position[3];
  float normal[3];
  float texcoords[2];
};
// we don't want no paddings
static_assert(sizeof(Vertex) == sizeof(float) * 8);

std::unique_ptr<Texture> Texture::create(size_t w, size_t h,
                                         std::span<const uint8_t> data) {
  GLuint texture;
  glGenTextures(1, &texture);

  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
               data.data());

  return std::unique_ptr<Texture>(new Texture{texture});
}

void Texture::bind() { glBindTexture(GL_TEXTURE_2D, texture); }

Texture::Texture(uint32_t texture) : texture{texture} {}

void Material::set_texture(std::shared_ptr<Texture> texture) {
  this->texture = texture;
}

void Material::bind() const {
  glUseProgram(program);
  if (texture) (*texture)->bind();
}

void Mesh::bind_vertex_buffer() const {
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
}

GLuint compile_shader(GLenum type, std::string_view code) {
  auto shader = glCreateShader(type);

  const GLchar* data = code.data();
  const GLint size = code.size();
  glShaderSource(shader, 1, &data, &size);

  glCompileShader(shader);

  GLint res;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &res);
  if (!res) {
    GLchar log[1024];
    glGetShaderInfoLog(shader, 1024, nullptr, log);

    std::cerr << "Failed to compile shader: " << log << "\n";
    throw std::runtime_error{"Failed to compile shader"};
  }
  return shader;
}

Material::Material(uint32_t program)
    : program{program},
      camera_position_location{
          glGetUniformLocation(program, "camera_position")},
      model_matrix_location{glGetUniformLocation(program, "model_matrix")},
      inv_model_matrix_location{
          glGetUniformLocation(program, "inv_model_matrix")} {}

void Material::update_model_matrix(
    const linalg::aliases::float4x4& matrix,
    const linalg::aliases::float4x4& inv_matrix) {
  glUniformMatrix4fv(model_matrix_location, 1, false,
                     reinterpret_cast<const float*>(&matrix));
  glUniformMatrix4fv(inv_model_matrix_location, 1, false,
                     reinterpret_cast<const float*>(&inv_matrix));
}

void Material::update_camera_position(linalg::aliases::float3 pos) {
  glUniform3f(camera_position_location, pos.x, pos.y, pos.z);
}

void InstancedMesh::render() {
  auto translation = linalg::translation_matrix(pos);
  auto rotation = linalg::rotation_matrix(this->rotation);
  auto inv_rotation = linalg::rotation_matrix(linalg::qinv(this->rotation));
  auto scale = linalg::scaling_matrix(this->scale);
  auto inv_scale = linalg::scaling_matrix(1.0f / this->scale);
  auto model = linalg::mul(translation, linalg::mul(rotation, scale));
  // no inv_translation since inv_model is used for normals which aren't tranlsated anyway
  auto inv_model = linalg::transpose(linalg::mul(inv_scale, inv_rotation));
  material->update_model_matrix(model, inv_model);

  mesh->bind_vertex_buffer();

  glVertexAttribPointer(static_cast<uint32_t>(AttributeLocation::Positions), 3,
                        GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
  glEnableVertexAttribArray(
      static_cast<uint32_t>(AttributeLocation::Positions));

  glVertexAttribPointer(static_cast<uint32_t>(AttributeLocation::Normals), 3,
                        GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (void*)(sizeof(float) * 3));
  glEnableVertexAttribArray(static_cast<uint32_t>(AttributeLocation::Normals));

  glVertexAttribPointer(static_cast<uint32_t>(AttributeLocation::TexCoords), 2,
                        GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (void*)(sizeof(float) * 6));
  glEnableVertexAttribArray(
      static_cast<uint32_t>(AttributeLocation::TexCoords));

  glDrawArraysInstanced(GL_TRIANGLES, 0, mesh->get_vertex_num(), 1);
}

std::shared_ptr<Renderer> Renderer::create(std::unique_ptr<Environment> env) {
  auto renderer = std::make_shared<Renderer>();
  renderer->env = std::move(env);
  renderer->aspect_ratio = renderer->env->aspect_ratio();

  auto vertex_shader = compile_shader(GL_VERTEX_SHADER, blinn_phong_vert_code);

  auto fragment_shader =
      compile_shader(GL_FRAGMENT_SHADER, blinn_phong_frag_code);

  renderer->program = glCreateProgram();

  glAttachShader(renderer->program, vertex_shader);
  glAttachShader(renderer->program, fragment_shader);
  glLinkProgram(renderer->program);

  GLint res{};

  glGetProgramiv(renderer->program, GL_LINK_STATUS, &res);
  if (!res) {
    GLchar log[1024];
    glGetProgramInfoLog(renderer->program, 1024, nullptr, log);
    std::cerr << "Failed to link shader program: " << log << "\n";
    throw std::runtime_error{"Failed to link shader program"};
  }

  renderer->view_proj_matrix_location =
      glGetUniformLocation(renderer->program, "view_proj_matrix");

  glUseProgram(renderer->program);

  renderer->main_material = std::make_shared<Material>(renderer->program);

  auto view_proj = linalg::perspective_matrix<float>(
      3.1415 / 3.0, renderer->aspect_ratio, 0.01, 100.0);
  // probably UB by standard
  glUniformMatrix4fv(renderer->view_proj_matrix_location, 1, false,
                     reinterpret_cast<float*>(&view_proj));

  glEnable(GL_DEPTH_TEST);
  glClearColor(0.5, 0.5, 1.0, 1.0);
  glClearDepthf(1.0);

  return renderer;
}

void Renderer::render_frame(double now_sec) {
  last_frame_start = now_sec;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(program);

  auto view_proj = linalg::mul(linalg::perspective_matrix<float>(
                                   3.1415 / 2.0, aspect_ratio, 0.01, 100.0),
                               camera.calc_view_matrix());
  // probably UB by standard
  glUniformMatrix4fv(view_proj_matrix_location, 1, false,
                     reinterpret_cast<float*>(&view_proj));

  main_material->update_camera_position(camera.get_position());

  for (auto& mesh : meshes) {
    mesh->render();
  }

  last_frame_end = env->time_now();
}

Renderer::~Renderer() {}

void Renderer::resize(double new_width, double new_height) {
  std::cerr << "Resize to " << new_width << ", " << new_height << "\n";
  aspect_ratio = new_width / new_height;
  glViewport(0, 0, new_width, new_height);
}

std::unique_ptr<Mesh> Renderer::load_mesh(const std::filesystem::path& path) {
  std::string warn;
  std::string err;

  tinyobj::ObjReaderConfig reader_config;
  reader_config.mtl_search_path = "./";
  tinyobj::ObjReader reader;

  if (!reader.ParseFromFile(path, reader_config)) {
    if (!reader.Error().empty()) {
      std::cerr << "TinyObjReader: " << reader.Error();
    }
    exit(1);
  }

  if (!reader.Warning().empty()) {
    std::cerr << "TinyObjReader: " << reader.Warning();
  }

  assert(reader.GetShapes().size() == 1);
  auto& shape = reader.GetShapes().at(0);

  std::vector<Vertex> vertices;

  for (auto& index : shape.mesh.indices) {
    auto& positions = reader.GetAttrib().vertices;
    auto vx = positions[index.vertex_index * 3];
    auto vy = positions[index.vertex_index * 3 + 1];
    auto vz = positions[index.vertex_index * 3 + 2];

    auto& normals = reader.GetAttrib().normals;
    auto nx = normals[index.normal_index * 3];
    auto ny = normals[index.normal_index * 3 + 1];
    auto nz = normals[index.normal_index * 3 + 2];

    auto& texcoords = reader.GetAttrib().texcoords;
    auto u = texcoords[index.texcoord_index * 2];
    auto v = texcoords[index.texcoord_index * 2 + 1];
    vertices.emplace_back(Vertex{
        .position = {vx, vy, vz}, .normal = {nx, ny, nz}, .texcoords = {u, v}});
  }

  GLuint vertex_buffer;
  glGenBuffers(1, &vertex_buffer);

  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);

  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex),
               vertices.data(), GL_STATIC_DRAW);

  return std::make_unique<Mesh>(vertices.size(), vertex_buffer);
}

std::shared_ptr<Texture> Renderer::load_texture(
    std::filesystem::path const& path) {
  auto texture = env->load_texture(path);
  for (auto byte: texture.data | std::views::take(128)) {
    std::cout << (int)byte << " ";
  }
  std::cout << "\n";
  return Texture::create(texture.w, texture.h, std::move(texture.data));
}

void Renderer::set_main_texture(std::shared_ptr<Texture> texture) {
  main_material->set_texture(texture);
}

std::shared_ptr<InstancedMesh> Renderer::instantiate_mesh(
    const std::shared_ptr<Mesh>& mesh) {
  meshes.push_back(std::make_shared<InstancedMesh>(mesh, main_material));
  return meshes.back();
}
