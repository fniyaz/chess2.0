#version 300 es

precision mediump float;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texcoords;
uniform mat4 model_matrix;
uniform mat4 inv_model_matrix;
uniform mat4 view_proj_matrix;

out vec3 world_position;
out vec3 world_normal;
out vec2 uvs;

void main() {
  gl_Position = view_proj_matrix * model_matrix * vec4(position, 1.0);
  world_position = (model_matrix * vec4(position, 1.0)).xyz;
  world_normal = normalize((inv_model_matrix * vec4(normal, 0.0)).xyz);
  uvs = texcoords;
}
