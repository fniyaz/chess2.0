#version 300 es

precision mediump float;

uniform vec3 camera_position;
uniform sampler2D albedo_sampler;

const vec3 sun_dir = vec3(-1.0, -1.0, -1.0);
const vec3 sun_color = vec3(1.0, 1.0, 1.0);
const float shininess = 32.0;
const float specular_strength = 1.0;

in vec3 world_position;
in vec3 world_normal;
in vec2 uvs; 

out vec4 out_color;

void main() { 
    vec3 light_dir = normalize(-sun_dir);
    float light_amount = clamp(dot(light_dir, world_normal), 0.0, 1.0);
    vec3 diffuse = sun_color * light_amount;

    vec3 eye_dir = normalize(camera_position - world_position);
    // vec3 reflection = reflect(light_dir, world_normal);
    vec3 halfway = normalize(eye_dir + light_dir);

    float specular_amount = pow(max(dot(world_normal, halfway), 0.0), shininess);
    vec3 specular = specular_strength * specular_amount * sun_color;

    vec4 mask = texture(albedo_sampler, uvs);
    vec3 albedo = mask.b * vec3(1.0, 1.0, 1.0);
    albedo += mask.g * vec3(0.0, 0.0, 0.0);
    albedo += mask.r * vec3(0.8, 0.6, 0.6);
    out_color = vec4(albedo * diffuse + specular, 1.0);
}
