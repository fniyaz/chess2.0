#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_opengl.h>

#include <cassert>
#include <iostream>
#include <optional>

#include "controller.hpp"
#include "renderer.hpp"

class DesktopEnvironment : public Environment {
 public:
  DesktopEnvironment(SDL_Window *window) : window{window} {}

  double time_now() const override {
    auto now_us = std::chrono::duration_cast<std::chrono::microseconds>(
                      std::chrono::steady_clock::now().time_since_epoch())
                      .count();
    return static_cast<double>(now_us) / 1e6;
  }

  float aspect_ratio() const override {
    int w, h;
    SDL_GL_GetDrawableSize(window, &w, &h);
    return static_cast<float>(w) / static_cast<float>(h);
  }

  Texture load_texture(std::filesystem::path const &path) const override {
    SDL_Surface *surface = IMG_Load(path.c_str());
    if (surface == nullptr) {
      std::cerr << "Failed to load texture from " << path << ": "
                << IMG_GetError() << "\n";
      throw std::runtime_error{"Texture load failed"};
    }
    SDL_Surface* rgba = SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_RGBA32, 0);
    std::vector<uint8_t> res;
    size_t size = rgba->w * rgba->h * rgba->format->BytesPerPixel;
    res.resize(size);
    std::copy_n(static_cast<uint8_t *>(rgba->pixels), size, res.begin());
    Texture texture{
        .w = static_cast<size_t>(rgba->w),
        .h = static_cast<size_t>(rgba->h),
        .format = 42,
        .data = std::move(res),
    };
    SDL_FreeSurface(surface);
    SDL_FreeSurface(rgba);
    return texture;
  }

 private:
  SDL_Window *window;
};

template <typename Duration>
double duration_to_sec(Duration dur) {
  return static_cast<double>(
             std::chrono::duration_cast<std::chrono::microseconds>(dur)
                 .count()) /
         1e6;
}

std::optional<Key> sdl_scancode_to_key(SDL_Scancode scan) {
  switch (scan) {
    case SDL_SCANCODE_0:
      return Key::Zero;
    case SDL_SCANCODE_1:
      return Key::One;
    case SDL_SCANCODE_2:
      return Key::Two;
    case SDL_SCANCODE_3:
      return Key::Three;
    case SDL_SCANCODE_4:
      return Key::Four;
    case SDL_SCANCODE_5:
      return Key::Five;
    case SDL_SCANCODE_6:
      return Key::Six;
    case SDL_SCANCODE_7:
      return Key::Seven;
    case SDL_SCANCODE_8:
      return Key::Eight;
    case SDL_SCANCODE_9:
      return Key::Nine;

#define CASE_LETTER(letter)   \
  case SDL_SCANCODE_##letter: \
    return Key::letter;
      CASE_LETTER(W)
      CASE_LETTER(A)
      CASE_LETTER(S)
      CASE_LETTER(D)
      CASE_LETTER(Q)
      CASE_LETTER(E)

    case SDL_SCANCODE_UP:
      return Key::ArrowUp;
    case SDL_SCANCODE_DOWN:
      return Key::ArrowDown;
    case SDL_SCANCODE_LEFT:
      return Key::ArrowLeft;
    case SDL_SCANCODE_RIGHT:
      return Key::ArrowRight;

    case SDL_SCANCODE_ESCAPE:
      return Key::Esc;
    case SDL_SCANCODE_SPACE:
      return Key::Space;
    case SDL_SCANCODE_RETURN:
      return Key::Enter;
    default:
      return std::nullopt;
  }
}

void relay_sdl_event_to_controller(Controller &controller,
                                   const SDL_Event &event) {
  switch (event.type) {
    case SDL_KEYDOWN: {
      if (auto key_opt = sdl_scancode_to_key(event.key.keysym.scancode)) {
        controller.process_keyboard_input(*key_opt, KeyState::Pressed);
      }
      break;
    }
    case SDL_KEYUP: {
      if (auto key_opt = sdl_scancode_to_key(event.key.keysym.scancode)) {
        controller.process_keyboard_input(*key_opt, KeyState::Released);
      }
      break;
    }
    case SDL_MOUSEMOTION:
      controller.process_mouse_move(event.motion.x, event.motion.y,
                                    event.motion.xrel, event.motion.yrel);
      break;
  }
}

int main() {
  SDL_Window *window =
      SDL_CreateWindow("OpenGL Test", 0, 0, 1600, 900, SDL_WINDOW_OPENGL);
  assert(window);

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
  SDL_GL_CreateContext(window);

  auto renderer =
      Renderer::create(std::make_unique<DesktopEnvironment>(window));

  Controller controller{renderer, "./assets"};

  controller.init_scene();

  int32_t running = 1;
  int32_t fullscreen = 0;

  std::chrono::steady_clock::duration delta_time{};
  while (running) {
    float delta_time_sec = duration_to_sec(delta_time);
    auto frame_start = std::chrono::steady_clock::now();
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      relay_sdl_event_to_controller(controller, event);
      if (event.type == SDL_KEYDOWN) {
        switch (event.key.keysym.sym) {
          case SDLK_ESCAPE:
            running = 0;
            break;
          case 'f':
            fullscreen = !fullscreen;
            if (fullscreen) {
              SDL_SetWindowFullscreen(
                  window, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN_DESKTOP);
            } else {
              SDL_SetWindowFullscreen(window, SDL_WINDOW_OPENGL);
            }
            break;
          default:
            break;
        }
      } else if (event.type == SDL_KEYUP) {
      } else if (event.type == SDL_QUIT) {
        running = 0;
      }
    }
    controller.update(delta_time_sec);
    renderer->render_frame(
        duration_to_sec(std::chrono::steady_clock::now().time_since_epoch()));
    SDL_GL_SwapWindow(window);
    delta_time = std::chrono::steady_clock::now() - frame_start;
  }
}