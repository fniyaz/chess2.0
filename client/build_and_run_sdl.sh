#!/usr/bin/env sh
set -xeuo pipefail

export CC=clang
export CXX=clang++
cmake -B build/client-sdl -S client -DTARGET=SDL -DCMAKE_BUILD_TYPE=Debug
cmake --build build/client-sdl --target SdlClient 
build/client-sdl/SdlClient