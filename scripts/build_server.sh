#!/usr/bin/env sh
set -xeuo pipefail

mkdir -p build
cmake -B build/server -S server
cmake --build build/server
