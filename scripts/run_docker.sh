#!/usr/bin/env sh
set -xeuo pipefail

docker container stop chess_2 || true
docker container rm chess_2 || true
docker run -d -p 80:80 --name chess_2 chess_2_server
