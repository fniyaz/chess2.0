#!/usr/bin/env sh
set -xeuo pipefail

mkdir -p build
emcmake cmake -B build/client -S client -DTARGET=Web 
cmake --build build/client
