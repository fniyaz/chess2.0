#!/usr/bin/env sh
set -xeuo pipefail

./scripts/build_server.sh
./scripts/build_client.sh
./scripts/build_docker.sh
./scripts/run_docker.sh
