cmake_minimum_required(VERSION 3.1...3.27)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

project(
  Test
  VERSION 1.0
  LANGUAGES CXX
)
set (CMAKE_CXX_STANDARD 20)

set(Boost_ADDITIONAL_VERSIONS 1.83.0 1.83)
find_package(Boost REQUIRED COMPONENTS coroutine)

add_executable(Server main.cpp)
target_link_libraries(Server Boost::boost Boost::coroutine)

