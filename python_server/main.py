from simple_websocket_server import WebSocketServer, WebSocket
import datetime


class SimpleChat(WebSocket):
    def handle(self):
        dp = [float(i) for i in str(self.data).split(',')]
        pos[0] += dp[0]
        pos[1] += dp[1]
        for client in clients:
            if client != self:
                client.send_message(prepare_pos(pos))

    def connected(self):
        print(datetime.datetime.now(), self.address, 'connected')
        clients.append(self)
        self.send_message(prepare_pos(pos))

    def handle_close(self):
        clients.remove(self)
        print(datetime.datetime.now(), self.address, 'closed')


clients = []
pos = [0.0, 0.0]

def prepare_pos(pos):
    return f"{pos[0]}, {pos[1]}"

server = WebSocketServer('0.0.0.0', 8090, SimpleChat)
server.serve_forever()

